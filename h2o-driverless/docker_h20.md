# h2o Driverless

https://www.h2o.ai/try-driverless-ai/

Docker can be started with

```
docker run -it --rm -p 12345:12345 -p 8888:8888 -p 5000:5000 h2oai/dai-centos7-x86_64:1.3.1-9.0
```

admin / password