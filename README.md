# Auto Modelling Overview

## Kaggle Competition used to test model performance

https://www.kaggle.com/c/house-prices-advanced-regression-techniques

Tested Frameworks:

## H20

https://www.h2o.ai/products/h2o-driverless-ai/

## TPOT

https://automl.info/tpot/

## AutoML SKLearn

https://github.com/automl/auto-sklearn

## AutoML Python

https://pypi.org/project/automl/
`ml_predictor.trained_final_model`

## MLBox

https://github.com/AxeldeRomblay/MLBox

## ADAnet

https://ai.googleblog.com/2018/10/introducing-adanet-fast-and-flexible.html

## AutoKeras

https://github.com/jhfjhfj1/autokeras

---

# Indirectly Related Frameworks

The following tools/projects are indirectly related with Automated Machine Learning. 

## featuretools

https://www.featuretools.com/

## XCessiv

https://github.com/reiinakano/xcessiv

## Heamy

https://github.com/rushter/heamy

## Feature-Stuff

https://github.com/hiflyin/Feature-Stuff