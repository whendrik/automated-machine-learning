# Docker Instructions

All notebooks should run with `tensorflow/tensorflow:latest-py3` docker, which provides a jupyter environment with python 3.

```
docker run -it --rm -p 8888:8888 -v "`pwd`":/notebooks/automated_ml tensorflow/tensorflow:latest-py3
```